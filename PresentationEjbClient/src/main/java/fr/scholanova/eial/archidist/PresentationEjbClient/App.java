package fr.scholanova.eial.archidist.PresentationEjbClient;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import fr.scholanova.eial.archidist.PresentationEjbInterface.CalculInterface;
import fr.scholanova.eial.archidist.PresentationEjbInterface.EjbTestEtat;
import fr.scholanova.eial.archidist.PresentationEjbInterface.MangerInterface;
import fr.scholanova.eial.archidist.PresentationEjbInterface.Repas;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {       
        Properties ppt = new Properties();
        ppt.put(Context.PROVIDER_URL, "http-remoting://127.0.0.1:8080");
        ppt.put(Context.SECURITY_PRINCIPAL, "admin");
        ppt.put(Context.SECURITY_CREDENTIALS, "admin");
        ppt.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        ppt.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        ppt.put("jboss.naming.client.ejb.context", true);
        
        InitialContext ctx = new InitialContext(ppt);
        
        CalculInterface calculInterface = (CalculInterface)ctx.lookup("PresentationEjbEAR-1.0/PresentationEjb-1.0/Calcul!fr.scholanova.eial.archidist.PresentationEjbInterface.CalculInterface");
        MangerInterface mangerInterface = (MangerInterface)ctx.lookup("PresentationEjbEAR-1.0/PresentationEjb-1.0/Manger!fr.scholanova.eial.archidist.PresentationEjbInterface.MangerInterface");
               
        EjbTestEtat proxySingleton = (EjbTestEtat)ctx.lookup("PresentationEjbEAR-1.0/PresentationEjb-1.0/EjbSingleton!fr.scholanova.eial.archidist.PresentationEjbInterface.EjbTestEtat");
        EjbTestEtat proxyStateless = (EjbTestEtat)ctx.lookup("PresentationEjbEAR-1.0/PresentationEjb-1.0/EjbStateless!fr.scholanova.eial.archidist.PresentationEjbInterface.EjbTestEtat");
        EjbTestEtat proxyStateful = (EjbTestEtat)ctx.lookup("PresentationEjbEAR-1.0/PresentationEjb-1.0/EjbStateful!fr.scholanova.eial.archidist.PresentationEjbInterface.EjbTestEtat");

        
        for (int i = 0; i < 10000; i++) {
			proxySingleton.increment();
			proxyStateful.increment();
		}
        
        Thread t1 = new Thread(() -> {
        	for (int i = 0; i < 10000; i++) {
				proxyStateless.increment();
			}
        });
        
        Thread t2 = new Thread(() -> {
        	for (int i = 0; i < 10000; i++) {
				proxyStateless.increment();
			}
        });
        
        Thread t3 = new Thread(() -> {
        	for (int i = 0; i < 10000; i++) {
				proxyStateless.increment();
			}
        });
        
        Thread t4 = new Thread(() -> {
        	for (int i = 0; i < 10000; i++) {
				proxyStateless.increment();
			}
        });
        
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        
        Thread.sleep(10000);
        
        System.out.println("Stateful : " + proxyStateful.getNb());
        System.out.println("Stateless : " + proxyStateful.getNb());
        System.out.println("Singleton : " + proxyStateful.getNb());
        
        long resAdd = calculInterface.add(10, 10);
        double resPow = calculInterface.power(2, 2);
        Repas repas = mangerInterface.mangerDehors();
        
        System.out.printf("10 + 10 = %s\n", resAdd);
        System.out.printf("2 ^ 2 = %s\n", resPow);
        System.out.printf("Repas: %s %s %s\n", repas.getName(), repas.getPrice(), repas.isOutside());
    }
}
