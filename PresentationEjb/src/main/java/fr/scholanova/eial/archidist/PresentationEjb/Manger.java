package fr.scholanova.eial.archidist.PresentationEjb;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.scholanova.eial.archidist.PresentationEjbInterface.MangerInterface;
import fr.scholanova.eial.archidist.PresentationEjbInterface.Repas;

@Remote(MangerInterface.class)
@Stateless
public class Manger implements MangerInterface {

	@Override
	public Repas mangerDehors() {
		// TODO Auto-generated method stub
		return new Repas("Tacos", 9.50, true);
	}

}
