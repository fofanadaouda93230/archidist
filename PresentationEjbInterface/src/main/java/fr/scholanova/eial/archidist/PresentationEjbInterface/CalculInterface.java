package fr.scholanova.eial.archidist.PresentationEjbInterface;

public interface CalculInterface {
	public int add(int i, int j);
	public double power(double i, double power);
}
