package fr.scholanova.eial.archidist.PresentationEjbInterface;

import java.io.Serializable;

public class Repas implements Serializable {

    private String name;
	private double price;
    private boolean outside;
    
    public Repas() {
    }

    public Repas(String name, double price, boolean outside) {
        // TODO Auto-generated constructor stub
    	super();
        this.name = name;
        this.price = price;
        this.outside = outside;
    }
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isOutside() {
		return outside;
	}

	public void setOutside(boolean outside) {
		this.outside = outside;
	}
}
