package fr.scholanova.eial.archidist.PresentationEjb;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.scholanova.eial.archidist.PresentationEjbInterface.EjbTestEtat;


@Remote(EjbTestEtat.class)
@Stateless
public class EjbStateless  implements EjbTestEtat{
	private int nb;
	
	@Override
	public void increment() {
		nb++;
	}

	@Override
	public int getNb() {
		return this.nb;
	}

}
